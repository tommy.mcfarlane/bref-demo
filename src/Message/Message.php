<?php

declare(strict_types=1);

namespace App\Message;

interface Message
{
    public static function fromArray(array $details): self;
}