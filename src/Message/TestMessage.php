<?php

declare(strict_types=1);

namespace App\Message;

final class TestMessage implements Message
{
    public const DETAIL_TYPE = 'event.published';

    public static function fromArray(array $details): self
    {
        return new self();
    }
}
