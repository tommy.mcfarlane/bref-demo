<?php

declare(strict_types=1);

namespace App\Subscriber\TestMessage;

use App\Message\TestMessage;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final class MySecondSubscriber
{
    public function __invoke(TestMessage $message): void
    {
        echo PHP_EOL.'Invoking '.__CLASS__;
    }
}