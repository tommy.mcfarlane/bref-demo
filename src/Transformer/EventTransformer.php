<?php

declare(strict_types=1);

namespace App\Transformer;

use App\Message\Message;
use App\Message\TestMessage;

final class EventTransformer
{
    /** @var list<string,Message> */
    private const MESSAGE_MAP = [
        TestMessage::DETAIL_TYPE => TestMessage::class,
    ];

    public function transform(string $detailType, array $details): Message
    {
        if (!array_key_exists($detailType, self::MESSAGE_MAP)) {
            throw new \RuntimeException('Event detail type not found.');
        }

        return self::MESSAGE_MAP[$detailType]::fromArray($details);
    }
}