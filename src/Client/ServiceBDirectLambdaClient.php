<?php

declare(strict_types=1);

namespace App\Client;

use Aws\Lambda\LambdaClient;
use Aws\Result;

final readonly class ServiceBDirectLambdaClient implements ServiceBClient
{
    private LambdaClient $client;

    public function __construct()
    {
        $this->client = new LambdaClient([
            'region'  => 'eu-west-2',
            'credentials' => [
                'key'    => 'AKIA3FLDZMFUXS66NUPM',
                'secret' => '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',
            ],
        ]);
    }

    public function getData(): Result
    {
        return $this->client->invoke([
            'FunctionName' => 'system-b-dev-direct-lambda-handler',
            'InvocationType' => 'RequestResponse',
            'LogType' => 'None',
            'output' => 'text',
            'cli-binary-format' => 'raw-in-base64-out',
            'Payload' => json_encode(
                [
                    'key_1' => 'request',
                    'key_2' => 'foo',
                    'key_3' => 'bar',
                ],
                JSON_THROW_ON_ERROR
            ),
        ]);
    }
}