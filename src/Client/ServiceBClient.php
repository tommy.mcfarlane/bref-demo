<?php

declare(strict_types=1);

namespace App\Client;

use Aws\Result;

interface ServiceBClient
{
    public function getData(): Result;
}