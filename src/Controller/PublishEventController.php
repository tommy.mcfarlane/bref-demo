<?php

declare(strict_types=1);

namespace App\Controller;

use App\Message\TestMessage;
use Aws\Credentials\Credentials;
use Aws\EventBridge\EventBridgeClient;
use Aws\Signature\SignatureV4;
use Bref\Event\EventBridge\EventBridgeEvent;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Attribute\Route;

class PublishEventController extends AbstractController
{
    public function __construct(
        private readonly MessageBusInterface $messageBus
    ) {
    }

    #[Route('/event')]
    public function index(): Response
    {
        $credentials = new Credentials('AKIA3FLDZMFUXS66NUPM', '4GDLgCusEZViPo8R4Z1MCe0SVkvuYgvATHiVUinq');
        $eventBridgeClient = new EventBridgeClient([
            'region' => 'eu-west-2',
            'version' => 'latest',
            'signature' => new SignatureV4('eventbridge', 'eu-west-2', ['use_v4a' => true]),
            'credentials' => $credentials
        ]);

        $event = [
            'Source' => 'symfony',
            'DetailType' => 'event.published',
            'Detail' => json_encode(['foo' => 'bar'], JSON_THROW_ON_ERROR),
            'Time' => (new \DateTime())->getTimestamp(),
            'Resources' => ['php-script'],
            'EventBusName' => 'default',
            'TraceHeader' => 'test'
        ];

        $result = $eventBridgeClient->putEvents([
            'EndpointId' => 'qm9cydilt0.roo',
            'Entries' => [$event]
        ]);
        echo 'Published to event bridge';

        $this->messageBus->dispatch(new TestMessage());
        echo 'Published to messabe bus - should be event bridge.';

        return $this->json(['published' => 'ok', 'result' => $result->toArray()]);
    }
}
