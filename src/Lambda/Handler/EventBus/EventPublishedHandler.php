<?php

declare(strict_types=1);

namespace App\Lambda\Handler\EventBus;

use App\Transformer\EventTransformer;
use Bref\Context\Context;
use Bref\Event\EventBridge\EventBridgeEvent;
use Bref\Event\EventBridge\EventBridgeHandler;
use Symfony\Component\Messenger\MessageBusInterface;

class EventPublishedHandler extends EventBridgeHandler
{
    public function __construct(
        private readonly MessageBusInterface $messageBus,
        private readonly EventTransformer $eventTransformer,
    ) {
    }

    public function handleEventBridge(EventBridgeEvent $event, Context $context): void
    {
        $message = $event->getDetail();
        $detailType = $event->getDetailType();
        echo 'This is the message: '. json_encode($message, JSON_THROW_ON_ERROR);
        echo PHP_EOL.'This is the detail type: '.(string) $detailType;
        $this->messageBus->dispatch(
            $this->eventTransformer->transform(
                $event->getDetailType(),
                $event->getDetail(),
            )
        );
    }
}