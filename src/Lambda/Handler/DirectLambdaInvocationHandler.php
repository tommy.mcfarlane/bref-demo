<?php

declare(strict_types=1);

namespace App\Lambda\Handler;

use App\Client\ServiceBClient;
use App\Service\MyService;
use Aws\Lambda\LambdaClient;
use Aws\Result;
use Bref\Context\Context;
use Bref\Event\Handler;

final readonly class DirectLambdaInvocationHandler implements Handler
{
    public function __construct(
        private readonly ServiceBClient $serviceBClient,
    ) {
    }

    public function handle(mixed $event, Context $context): array
    {
        $event = $this->resolveEvent($event); // validate the payload

        $response = $this->serviceBClient->getData();
        $responseArray = $response?->toArray();
        $contentsRaw = $response->get('Payload')->getContents();
        $payload2 = json_decode($contentsRaw, true, 512, JSON_THROW_ON_ERROR);
        $logResult = $responseArray['LogResult'] ?? [];
        $statusCode = $responseArray['StatusCode'] ?? [];
        $functionError = $responseArray['FunctionError'] ?? [];
        unset($responseArray['Payload'], $responseArray['LogResult'], $responseArray['StatusCode'], $responseArray['FunctionError']);

        return [
            'event' => $event,
            'class' => __CLASS__,
            'response_data' => $payload2,
            'response_data_raw' => $contentsRaw,
            'status_code' => $statusCode,
            'function_error' => $functionError,
            'log_result' => base64_decode($logResult),
            'everything_else' => $response,
        ];
    }

    private function resolveEvent(mixed $event): array
    {
        // this method returns an object & validates the payload.
        return (array) $event;
    }
}