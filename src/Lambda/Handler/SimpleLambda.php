<?php

declare(strict_types=1);

namespace App\Lambda\Handler;

use App\Service\MyService;
use Bref\Context\Context;
use Bref\Event\Handler;

final readonly class SimpleLambda implements Handler
{
    public function __construct(private MyService $myService)
    {
    }

    public function handle(mixed $event, Context $context): array
    {
        return [
            'class' => __CLASS__,
            'event' => $event,
            'service' => get_class($this->myService),
        ];
    }
}