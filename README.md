# Bref demo

- Bref - https://bref.sh/docs
- Serverless - https://www.serverless.com/framework/docs
- Boto - https://boto.cloudhackers.com/en/latest/index.html

## Local development:
- https://bref.sh/docs/local-development/event-driven-functions

### Local lambda execution
- `serverless bref:local -f lambda --data='{"name": "John"}'`

### Local CLI command execution
- `serverless bref:local -f lambda --data='{"name": "John"}'`
- `docker compose exec php bin/console <<COMMAND_TO_EXECUTE>>` 

## Serverless CLI commands
- `serverless invoke -f <<FUNCTION_NAME>> --data='{"name": "John"}'` - invoke lambda
- `serverless bref:cli --args="app:upload_photo_similarity_pdf_to_s3"` - invoke CLI command

## Logging into ECR



```bash
# Populates your local env with AWS environment variables. These credentials are valid for 12 hours.
$ awsume build-tooling
Enter MFA token: <your mfa token here>
Session token will expire at 2021-09-22 05:27:15
[build-tooling] Role credentials will expire 2021-09-21 18:27:15

$ aws ecr get-login-password | docker login --username AWS --password-stdin 346121089040.dkr.ecr.eu-west-2.amazonaws.com
Login Succeeded
```

## Setting up your project

- Create local configuration files
- Create your config file by coping `build/local/.env.example` to `build/local/.env`
- Retrieve your github token
- Run command:
```bash
$ GITHUB_TOKEN="your-github-token-here" GITLAB_TOKEN="your-gitlab-token-here" make setup
```
Note that the GitLab and GitHub token can be set in your local ~/.composer/auth.json file:
```text
┌someone@someones-MBP ~/c/c/bankconnect (master)
└> cat ~/.composer/auth.json
{
    "bitbucket-oauth": {},
    "github-oauth": {
        "github.com": "ghp_xxxxxxx"
    },
    "gitlab-oauth": {},
    "gitlab-token": {
        "gitlab.com": "xxxxx-xxxxxxxx"
    },
    "http-basic": {},
    "bearer": {}
}
```

## Setup project if you want to simulate AWS sqs ([localstack](https://github.com/localstack/localstack))
on your local machine (the local-stack ui will available here: [http://localhost:8055](http://localhost:8055))
`make setup-with-localstack`

## Generate swagger documentation on local machine [http://localhost:1080/docs/](http://localhost:1080/docs/)
`make swagger`

## Testing.
`make test`
[https://behat-api-extension.readthedocs.io/en/latest/](https://behat-api-extension.readthedocs.io/en/latest/)

## TPP

#### Refresh tokens
[https://docs.truelayer.com/#renew-the-access_token](https://docs.truelayer.com/#renew-the-access_token)


#### Delete connection tokens
[https://docs.truelayer.com/#delete-encrypted-credentials](https://docs.truelayer.com/#delete-encrypted-credentials)

#### Error codes
[https://docs.truelayer.com/#error-codes](https://docs.truelayer.com/#error-codes)


## GitLab CI

### Clearing and resetting Database

- There is a step in the pipeline called 'Reset Database' which is in the development stage. This can be manually ran, and will truncate all of the Bank Connect tables in that environment, and then reseed with sample data.

- Once this is run, there is another manual step called 'Fetch banks', which will get the banks from the real Truelayer as the mock service doesn't currently support this.

