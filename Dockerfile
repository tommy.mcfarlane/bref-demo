FROM bref/php-82-fpm:2

COPY --from=bref/extra-redis-php-82:1 /opt /opt
COPY --from=bref/extra-gmp-php-82:1 /opt /opt

COPY . /var/task

#RUN docker-php-ext-install awscrt
RUN ["chmod", "+x", "/bin/bash"]
RUN ["chmod", "+x", "public/index.php"]

ENTRYPOINT ["/bin/bash", "-l", "-c"]

# Configure the handler file (the entrypoint that receives all HTTP requests)
CMD ["public/index.php"]
